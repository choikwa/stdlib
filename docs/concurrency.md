% tethercode docs > Concurrency

<!--
    This file is part of the tethercode documentation.
    Licensed under
    CC-By-4.0 https://creativecommons.org/licenses/by/4.0/
    Attribution:
    tethercode dev team https://tethercode.io/go/authors
-->

[Return to the documentation index](index.md)

# Concurrency

Tethercode's concurrency has a few basic principles.

The basic standpoint of tethercode is to support true but simple concurrency
wherever possible, but maintaining a stable interpreter state and data
consistency always takes precedence.

* *Simple shared data:*
  All variables can be accessed from all threads without any special
  mechanisms to keep sharing data simple.

* *Strive to support true concurrency:*
  When possible and reasonable, true hardware threads are
  preferred over single-thread coroutines.

* *Always maintain data consistency:*
  When something is altered in a single statement, another thread that does
  concurrent access will always see the old value or the new value.

* *Don't crash unless it can't be helped:*
  Unless an external C library corrupts memory due to threading issues,
  the byte code interpreter will always try to maintain a consistent state
  and raise proper errors if the concurrency confuses your code - and the
  standard library will generally be thread-safe unless noted otherwise in
  the manual.

This means threading is simple to use (with the "async" keyword) and
so is sharing data between threads, but this results in some performance
trade-offs.

