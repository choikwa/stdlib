% tethercode docs

<!--
    This file is part of the tethercode documentation.
    Licensed under
    CC-By-4.0 https://creativecommons.org/licenses/by/4.0/
    Attribution:
    tethercode dev team https://tethercode.io/go/authors
-->

[Return to the documentation index](index.md)

# Community

There are multiple places to get in touch with other people who use or work
on tethercode if you are stuck with a problem, or if you simply want to
speak about related topics.

## Official mailing list

The official discussion mailing list is for all questions, requests and
discussions all around the Wobbly language and associated tools:

 [Official mailing list subscription page](
   https://lists.wobblylang.org/mailman3/lists/discuss.lists.wobblylang.org/)
FIXME: adapt link

**Subscribe to the list via the link above**, and then you can both share new
topics by mailing to discuss@wobblylang.org, and answer to responses by
others by replying to the mails you get from that mailing list address.

If you are interested, there is also [an announcement list for latest
    releases](
    https://lists.wobblylang.org/mailman3/lists/announce.lists.wobblylang.org/
    ).

## Online chat

We have a chatroom on IRC at:

[Chat widget link - click to open the chat right away](
    http://webchat.freenode.net/?randomnick=1&channels=%23tethercode&uio=OT10cnVlJjExPTM1OQ96)

If you already have an IRC client and you want to connect without the web
widget, the details are: server: irc.freenode.net, room: #tethercode

While some off-topic discussion and socializing is appreciated, it is still
mainly intended as a help channel. Also, please keep in mind the basic
[community guidelines for behavior in the contributor's guide](
    ../CONTRIBUTING.md).


