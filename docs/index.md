% tethercode docs

<!--
    This file is part of the tethercode documentation.
    Licensed under
    CC-By-4.0 https://creativecommons.org/licenses/by/4.0/
    Attribution:
    tethercode dev team https://tethercode.io/go/authors
-->

# Documentation

This is the documentation of the tethercode programming language.

[Get the language / tools here!](https://tethercode.io/#get)

[Start reading here for a basic introduction.](tutorials/getting-started.md)

**Note:**
If you are looking at the markdown sources of the documentation right now
instead of the colorful HTML/browser version, use the command ```make docs```
in the repository folder to build the HTML version.

## Available topics

### Getting started
- **Getting started**: [how to set up tethercode to be able to test
    and run code](tutorials/getting-started.md).
- **Beginner's tutorials**: [simple tutorials to get started with tethercode
    as a newcomer](basic-tutorials.md)
- **Advanced tutorials**: [advanced tutorials showing how to do various
    more in-depth common projects like a server](advanced-tutorials.md)

### Reference
- **Specification**: [basic language syntax and formal definition](specification.md)
- **Concurrency**: [explanation of the concurrent behavior of wobbly code](
    concurrency.md)

## Misc information

### I am absolutely stuck, help me!
If you are writing something in tethercode and you can't get it to work and you
require external help, you might wanna check the official
 [discussion mailing list](https://lists.wobblylang.org).
There are also [other places to ask for help](community.md).

### Where to get it?
Get the latest version of the tethercode programming language development
tools from the [official website](https://tethercode.io), and
the current source code from the [public code repository](
    https://gitlab.com/tethercode/stdlib)!

### I found a bug!
If you find any bugs or encounter any issues, please check the
[contribution guide's section on how to report a bug](../CONTRIBUTING.md).
You are also free to report issues with this documentation or the official
website at [https://tethercode.io](https://tethercode.io).

### Other useful tools
This list will be expanded once we find nice ones!
If you wrote one and want it added here, please tell us on the
[mailing list](https://lists.wobblylang.org).

