" Vim syntax file
" Language: tethercode
" Maintainer: Jonas Thiem
" Latest Revision: 21 March 2017

if exists("b:current_syntax")
  finish
endif

" Lambda function:
syntax match TethercodeOperator "\v[^=]-\>"

" Strings
syntax region TethercodeString start=+\v\"+ skip=+\M\\\\\|\\"+ end=+\v\"+
syntax region TethercodeString start=+\v'+ skip=+\M\\\\\|\\'+ end=+\v'+

" Various errors
syntax match TethercodeError "\v\&\&|\|\||![^=]|;|\~|(\=|\>|\<|!)(\=\=)@=|[\=]@<=\>|[\=]@<=\<"
" : is allowed in slicing: [:..]

" Keywords
syntax keyword TethercodeBasicLanguageKeywords class extends func if while for else elseif let in assert throw try catch as finally
syntax keyword TethercodeBranch continue break return
syntax keyword TethercodeIdentifier self null TypeException IOException ValueException RuntimeException super async
syntax match TethercodeIdentifier "\vmath\.max"
syntax match TethercodeIdentifier "\vmath\.min"
syntax keyword TethercodeSpecialKeywords import multiarg preccheck_if preccheck_else preccheck_end
syntax keyword TethercodeBoolean true false
syntax keyword TethercodeConditional and or not
syntax cluster TethercodeKeywords contains=TethercodeBasicLanguageKeywords,TethercodeBranch,TethercodeIdentifier,TethercodeSpecialKeywords,TethercodeBoolean,TethercodeConditional

" Numbers
syntax match TethercodeVarLabel "\v(:)@![a-zA-Z_][0-9a-zA-Z_]+"
syntax match TethercodeNumber "\v-?[0-9]+|0x[0-9a-fA-F]+"

" Special attributes
syntax match TethercodeAttribute "\v:(const|unmanaged)([a-zA-Z0-9_])@!"

" Built-in funcs which can also be used as member variables
syntax match TethercodeBuiltinFunc "\v(\.|[a-zA-Z0-9_])@<!(type|hasmember|tostring|tonumber|print|char|ord)[a-zA-Z0-9_]@!" containedin=TethercodeVarLabel

" Operators
syntax match TethercodeOperator "\v(\+|\-[^\>]@=|\*|\/)"
syntax match TethercodeOperator "\v[^=]@!(\=|\=\=)[^=]@="
syntax match TethercodeOperator "\v\!\=[^=]@="
syntax match TethercodeOperator "\v([^=][ \n\r\t])@<=\>(([ \n\r\t]*[^=\}])|\=([^=]|\n|\r))@="
syntax match TethercodeOperator "\v(([^={][ \n\r\t]*)@<=)\<(\=?([^=]|\n|\r))@="

" Brackets
syntax match TethercodeBracket "\v(\{\<?|\[|\(|\)|\]|\>?\})"

" Comments
syntax match TethercodeLineComment "#.*" contains=@spell

let b:current_syntax = "tethercode"

hi def link TethercodeBasicLanguageKeywords Keyword
hi def link TethercodeString String
hi def link TethercodeBuiltinFunc Identifier
hi def link TethercodeIdentifier Identifier
hi def link TethercodeBranch Conditional
hi def link TethercodeLineComment Comment
hi def link TethercodeSpecialKeywords Special
hi def link TethercodeAttribute Special
hi def link TethercodeBoolean Boolean
hi def link TethercodeOperator Operator
hi def link TethercodeConditional Conditional
hi def link TethercodeNumber Number
hi def link TethercodeError Error
hi def link TethercodeBracket Special

