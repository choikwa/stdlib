
# tethercode stdlib - wildcard.tc
# Copyright (c) 2016-2017,
# tethercode dev team (see AUTHORS.md / https://tethercode.io/go/authors )
#
# This software is provided 'as-is', without any express or implied
# warranty. In no event will the authors be held liable for any damages
# arising from the use of this software.
#
# Permission is granted to anyone to use this software for any purpose,
# including commercial applications, and to alter it and redistribute it
# freely, subject to the following restrictions:
#
# 1. The origin of this software must not be misrepresented; you must not
#    claim that you wrote the original software. If you use this software
#    in a product, an acknowledgement in the product documentation would be
#    appreciated but is not required.
# 2. Altered source versions must be plainly marked as such, and must not be
#    misrepresented as being the original software.
# 3. This notice may not be removed or altered from any source distribution.
#
# SPDX-License-Identifier: Zlib

import encoding
import path

class _MatchCheckJob {
    let wildcard
    let string
    let schedule_func
    let wildcard_eat_length_tried = -1
    let is_path_wildcard

    func init(wildcard, string, schedule_func,
            is_path_wildcard=false) {
        self.schedule_func = schedule_func
        self.wildcard = wildcard
        assert(type(self.wildcard) == BaseType.bytes or
            type(self.wildcard) == BaseType.string)
        self.string = string
        self.is_path_wildcard = is_path_wildcard
    }

    func tostring {
        return ("<_MatchCheckJob('" +
            tostring(self.wildcard).replace("'", "'\"'\"'") + "," +
            tostring(self.string).replace("'", "'\"'\"'") + ")>")
    } 

    func process {
        let k = 0
        let i = 0
        # Compare byte by byte until we encounter a wildcard:
        while k < self.wildcard.length and i < self.string.length {
            if self.wildcard[k] != self.string[i] or
                    self.wildcard[k] == b"*" {
                if self.wildcard[k] != b"*" {
                    return false
                }

                # Check if we should eat up any directory separators:
                let eat_up_dir_separator = true
                if self.is_path_wildcard {
                    eat_up_dir_separator = false
                    if k + 1 < self.wildcard.length {
                        if self.wildcard[k + 1] == b"*" {
                            eat_up_dir_separator = true
                            k += 1
                        }
                    }
                }

                # (Performance) If this is the last *, this matches trivially:
                if k == self.wildcard.length - 1 and
                        eat_up_dir_separator {
                    return true
                }

                # Check how much the wildcard should eat up:
                let eat_up = self.wildcard_eat_length_tried + 1
                self.wildcard_eat_length_tried = eat_up
                if eat_up > self.string.length + i {
                    # We can't eat up that much. Return failure
                    return false
                }
                if (self.string[i + eat_up] == path.separator_bytes() or
                        self.string[i + eat_up] == b"/") and
                        not eat_up_dir_separator {
                    # This can't be eaten up. Return failure
                    return false
                }
                # Schedule a job to test this:
                self.schedule_func(self.wildcard[k + 1:],
                    self.string[i + eat_up:])

                # Return that we are still undecided:
                return null
            }
            k += 1
            i += 1
        }
        if k >= self.wildcard.length and i >= self.string.length {
            return true
        }
        # Remains of wildcard or string mismatch in length.
        return false
    }
}

class _ScheduledMatchCheckJob {
    let scheduled_requester
    let job
    func init(job, scheduled_requester) {
        self.job = job
        self.scheduled_requester = scheduled_requester
    }

    func tostring() {
        return "<_ScheduledMatchCheckJob job: " + tostring(self.job) + ">"
    }
}

## This internal class implements the matching for the match() wildcard
## matching function.
##
## It implements a recursive algorithm as a queue (to avoid actual deep
## recursion to avoid exceeding stack space) which checks if the given
## wildcard matches.
class _Matcher {
    let wildcard
    let string
    let queue = []
    let waiting_for_results_queue = []
    let current_requester = null
    let is_path_wildcard

    func init(wildcard, string, is_path_wildcard=false) {
        if type(wildcard) == BaseType.string and
                type(string) == BaseType.string {
            wildcard = encoding.encode(wildcard)
            string = encoding.encode(string)
        }
        assert(type(wildcard) == BaseType.bytes or
            type(wildcard) == BaseType.string)
        self.is_path_wildcard = is_path_wildcard
        self.wildcard = wildcard
        self.string = string
        #print("STARTING _Matcher: wildcard: " +
        #    tostring(self.wildcard) + ", string: " +
        #    tostring(self.string) + ", is_path_wildcard: " +
        #    tostring(self.is_path_wildcard))
    }
    func _schedule_function(wildcard, string) {
        assert(type(wildcard) == BaseType.bytes or
            type(wildcard) == BaseType.string)
        let job = _MatchCheckJob(wildcard, string, self._schedule_function,
            is_path_wildcard=self.is_path_wildcard)
        self.queue.add(_ScheduledMatchCheckJob(
            job, self.current_requester))
    }
    func run() {
        # Queue initial job on entire pattern:
        assert(type(self.wildcard) == BaseType.bytes or
            type(self.wildcard) == BaseType.string)
        let initial_job = _MatchCheckJob(self.wildcard, self.string,
            self._schedule_function, is_path_wildcard=self.is_path_wildcard)
        let initial_job_scheduled = _ScheduledMatchCheckJob(
            initial_job, null)
        self.queue.add(initial_job_scheduled)

        assert(type(self.wildcard) == BaseType.bytes or
            type(self.wildcard) == BaseType.string)
        # Process job queue until a definite result is known:
        while self.queue.length > 0 or
                self.waiting_for_results_queue.length > 0 {
            # If main queue is empty, get one from the waiting for results:
            if self.queue.length == 0 {
                self.queue.add(self.waiting_for_results_queue[
                    self.waiting_for_results_queue.length - 1])
                self.waiting_for_results_queue = (
                    self.waiting_for_results_queue[
                        :self.waiting_for_results_queue.length - 1])
            }

            # Process first job:
            let scheduled_job = self.queue[0]
            self.queue = self.queue[1:]
            #print("SCHEDULED JOB: " + tostring(scheduled_job))
            self.current_requester = scheduled_job
            let result = scheduled_job.job.process()
            #print("RESULT: " + tostring(result))

            # Evaluate result:
            if result == true {
                # If any job ever returns true, it definitely reached the
                # end of the pattern successfully without any doubts which
                # means we can instantly return success:
                return true
            } elseif result == false {
                # If this was the initial job, return result:
                if scheduled_job.job == initial_job {
                    return false
                }

                # Propagate result up by requeuing requester:
                if scheduled_job.scheduled_requester != null {
                    self.waiting_for_results_queue.add(
                        scheduled_job.scheduled_requester)
                }
            }
        }
        throw RuntimeException("reached end of job queue, " +
            "but no result has been decided upon (this should never happen)")
    }
}

## Check if the given wildcard maches the given unicode string or
## bytestring value. (both wildcard and value must be same type)
## Returns true if it matches, false otherwise.
##
## A wildcard can contain regular letters, numbers, any other
## unicode character, and a * placeholder. The path wild card
## also supports a special ** placeholder.
##
## If is_path_wildcard is set to false, * can eat up any amount and
## type of character in the matched string (e.g. a wildcard "abc*"
## matches a string "abcd", or "abcdefjaijgieogj", ..).
##
## If is_path_wildcard is set to true, * can eat up any amount of
## characters of any type except a directory separator. Use a **
## placeholder to eat up everything including directory separators.
##
## In addition, if is_path_wildcard is set to true, a leading / will
## have a special meaning:
## 1. If the wildcard has a leading /, the wildcard will be treated as
##    exact path wildcard.
##    This means it will match strings as usual, except it can also
##    match strings not starting with a directory separator.
## 2. If the wildcard has no leading /, it will be treated as filename
##    wildcard. It will be matched against the full string, and also any
##    component subsets starting from any directory separator later in
##    the full string and ending at the end of the string (not including
##    a trailing / in the string).
func match(wildcard, value, is_path_wildcard=false, debug=false) {
    # A few sanity checks:
    if type(wildcard) == BaseType.string and
            type(value) == BaseType.string {
        wildcard = encoding.encode(wildcard)
        value = encoding.encode(value)
    }
    if type(wildcard) == BaseType.string and
            type(value) != BaseType.string {
        throw ValueException("unicode wildcard cannot be checked against " +
            "non-unicode value")
    }
    if type(wildcard) == BaseType.bytes and
            type(value) != BaseType.bytes {
        throw ValueException("bytes wildcard cannot be checked against " +
            "non-bytes value")
    }

    # For path wildcards, omit trailing / in wildcard:
    if is_path_wildcard and wildcard.endswith(path.separator_bytes()) {
        wildcard = wildcard[:wildcard.length -
            path.separator_bytes().length]
    } elseif is_path_wildcard and wildcard.endswith(b"/") {
        wildcard = wildcard[:wildcard.length - 1]
    }

    # For path wildcards, don't compare against last / in checked value:
    if is_path_wildcard and value.endswith(path.separator_bytes()) {
        value = value[:value.length - path.separator_bytes().length]
    } elseif is_path_wildcard and value.endswith(b"/") {
        value = value[:wildcard.length - 1]
    }

    # For path wildcards, wildcard /xyz matches "xyz", and "xyz/...":
    let path_wildcard_no_subpart_matching = false
    if is_path_wildcard and (wildcard.startswith(path.separator_bytes())
            or wildcard.startswith(b"/")) {
        path_wildcard_no_subpart_matching = true
        if not value.startswith(b"/") and not value.startswith(
                path.separator_bytes()) {
            if wildcard.startswith(path.separator_bytes()) {
                wildcard = wildcard[path.separator_bytes().length:]
            } else {
                wildcard = wildcard[1:]
            }
        }
    }

    func remove_path_component(value, from_end=false) {
        # First, try to find b"/":
        let next_sep
        if not from_end {
            next_sep = value.find(b"/")
        } else {
            next_sep = value.rfind(b"/")
        }
        let sep_len = 1

        # Check against path.separator_bytes():
        let _path_sep
        if not from_end {
            _path_sep = value.find(path.separator_bytes())
        } else {
            _path_sep = value.rfind(path.separator_bytes())
        }

        # See what gave the better result:
        if _path_sep >= 0 and (
                    (not from_end and (_path_sep < next_sep or
                    next_sep < 0)) or
                    (from_end and (_path_sep > next_sep or
                    next_sep < 0))
                ) {
            next_sep = _path_sep
            sep_len = path.separator_bytes().length
        }
        if next_sep < 0 {  # No components left to remove.
            return value
        }

        # Truncate component:
        if not from_end {
            value = value[next_sep + sep_len:]
        } else {
            value = value[:next_sep]
        }

        return value
    }

    # Try to do regular full match:
    let matcher = _Matcher(wildcard, value, is_path_wildcard=is_path_wildcard)
    let result = matcher.run()
    if debug {
        print("wildcard.tc: debug: full match of '" + tostring(wildcard) +
            "' on '" +
            tostring(value) + "': " + tostring(result))
    }
    if result or path_wildcard_no_subpart_matching or not is_path_wildcard {
        if not result and is_path_wildcard and
                path_wildcard_no_subpart_matching {
            if debug {
                print("wildcard.tc: debug: trying gradual " +
                    "removal of end parts...")
            }
            # Match /xyz against "xyz/...". To do that, gradually remove parts
            # of the value from the end and see if it matches:
            # (since full value didn't match against wildcard)
            while true {
                # Remove last component from path:
                let old_value = value
                value = remove_path_component(value, from_end=true)
                if value.length == old_value.length {
                    return false
                }

                # Check new sub path:
                let matcher = _Matcher(wildcard, value,
                    is_path_wildcard=is_path_wildcard)
                let result = matcher.run()
                if debug {
                    print("wildcard.tc: debug: sub match of '" +
                        tostring(wildcard) + "' on '" +
                        tostring(value) + "': " +
                        tostring(result))
                }
                if result {
                    return true
                }
            }
        }
        return result
    }

    # This is a non-exact match path wildcard which can be matched against
    # path subparts.
    # Gradually remove path component by path component from the start of the
    # value and see if it matches
    # (since full value didn't match against wildcard)
    if debug {
        print("wildcard.tc: debug: trying gradual " +
            "removal of start parts...")
    }
    while true {
        # Remove first component from path:
        let old_value = value
        value = remove_path_component(value, from_end=false)
        if value.length == old_value.length {
            return false
        }
        
        # Check new sub path:
        let matcher = _Matcher(wildcard, value,
            is_path_wildcard=is_path_wildcard)
        let result = matcher.run() 
        if debug {
            print("wildcard.tc: debug: sub match of '" +
                tostring(wildcard) + "' on '" +
                tostring(value) + "': " +
                tostring(result))
        }
        if result {
            return true
        }
    }
}

