
# [tethercode](https://tethercode.io)

**CAUTION**: *SUPER UNFINISHED, DONT USE YET.*

Welcome to the [tethercode](https://tethercode.io) programming language!
Why it's cool:

- **Simple**: simple syntax combined with mature OOP as seen in Python

- **Productive**: dynamic types offer productivity, but lexical scoping
                  offers comprehensive error checking

- **Easy to ship:** create self-contained binaries for independent
                    applications

Also, tethercode can do this awesome thing:

- **Sandbox**: easily run untrusted code with fine-grained time and memory
               limits

This makes it very suitable for powerful, yet safe plugin architectures, or
the construction of coding games, strongly moddable multiplayer games or
other online platforms where untrusted code needs to be run.


## How does it look like?

tethercode has a syntax inspired by C, Python and Lua:

```tethercode
func main {
    print("Hello World!")
}
```

A more elaborate example which prints a random text snippet when
the program is run:

```tethercode
import random

# A collection of text snippets to display when the program is run:
let text_snippets = [
    "A gnome with a red hat waves to you in excitement!",
    "A gnome with a blue hat waves to you in excitement!",
    "A gnome with a green hat waves to you in excitement!"
]

func main {
    # Display a random text snippet:
    let random_snippet = text_snippets[random.rand_int(0, 3)]
    print(random_snippet)
}
```

You can find more advanced examples in the
[tutorials section of the documentation](docs/tutorials.md)!


## How to use

You can get the SDK for all major platforms here:
- [Download page for tethercode SDK & tools](https://tethercode.io/go/get)

Also, look at those tutorials to get started:

- Basic tutorial 1 **FIXME**
- [Find more help in the tutorials section of the documentation](
  docs/tutorials.md)


## Help & documentation

For more technical knowledge, take a look at the
  [full documentation](
  docs/index.md)!

If you are completely stuck, try the [mailing list](
    https://lists.wobblylang.org/mailman3/lists/discuss.lists.wobblylang.org/)
or the [community chat](
    http://webchat.freenode.net/?randomnick=1&channels=%23tethercode&uio=OT10cnVlJjExPTM1OQ96).

If you found a bug in tethercode, please let us now!
The [contribution guide](CONTRIBUTING.md) explains how to report a bug.


## Sandboxing

tethercode was specifically made for simple & low-overhead user script sandboxing
with versatile control over execution time and resource limits.

You can read more about it here: [Sandboxing guide](docs/sandboxing.md)

**Important Security Notice:**
while the language has been written with the use of sandboxing in mind,
there is no guarantee for security or safety. The sandbox of this language is
per design inherently more at risk of having security holes than for example
sophisticated virtual machine virtualization. tethercode has been written for
efficient, easy-to-use and low overhead sandboxing, but this comes at a cost.

Always remember:

- **you use this software at your own risk (also see License below)**

- always run the latest version of tethercode for the latest fixes

- security also depends on *your code* using the sandboxing features correctly

**If you find issues that impact the ability to sandbox safely, report them
  as soon as possible (and make sure to mark them as "confidential").**
Read the [contribution guide](CONTRIBUTING.md) on how to report a bug.


## License

tethercode is free software licensed under zlib (code) and
CC-BY-4.0 (documentation), see [LICENSE.md](LICENSE.md) for details.
*(With the exception of the logo which is under more restrictive terms
to make unofficial distributions more obvious. If in doubt, you can
turn the source archive into fully free software by removing the
`misc` folder)*

Check out [AUTHORS.md](AUTHORS.md) for a complete list of authors.


## Contribute

tethercode is open-source with all code, development activity and bug tickets
being open [on the official repository page](
    https://gitlab.com/tethercode/stdlib).
If you want to contribute and help out,
[read the contribution guide](CONTRIBUTING.md)!


### Developer contribution details

If you want to work on the tethercode compiler `tethercc`, standard library
etc., please consult [the installation instructions](INSTALL.md) for
bootstrapping them out of this source code package. If you are a regular
user and not trying to run an experimental developer version of tethercode, we
recommend that you [go to the official download page instead](
    https://tethercode.io/go/get).


