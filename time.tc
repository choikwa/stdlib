
# tethercode stdlib - time.tc
# Copyright (c) 2017,
# tethercode dev team (see AUTHORS.md / https://tethercode.io/go/authors )
#
# This software is provided 'as-is', without any express or implied
# warranty. In no event will the authors be held liable for any damages
# arising from the use of this software.
#
# Permission is granted to anyone to use this software for any purpose,
# including commercial applications, and to alter it and redistribute it
# freely, subject to the following restrictions:
#
# 1. The origin of this software must not be misrepresented; you must not
#    claim that you wrote the original software. If you use this software
#    in a product, an acknowledgement in the product documentation would be
#    appreciated but is not required.
# 2. Altered source versions must be plainly marked as such, and must not be
#    misrepresented as being the original software.
# 3. This notice may not be removed or altered from any source distribution.
#
# SPDX-License-Identifier: Zlib

preccheck_if translator_name == "baseline"
import pythonlang
pythonlang.do_import("time", "_python_time")
preccheck_end

func sleep(seconds) {
    preccheck_if translator_name == "baseline"
    _python_time.sleep(seconds)
    preccheck_else
    throw RuntimeException("code path not implemented")
    preccheck_end
}

func now {
    preccheck_if translator_name == "baseline"
    return _python_time.now()
    preccheck_else
    throw RuntimeException("code path not implemented")
    preccheck_end
}

func monotonic_ts {
    preccheck_if translator_name == "baseline"
    return _python_time.monotonic()
    preccheck_else
    throw RuntimeException("code path not implemented")
    preccheck_end
}
