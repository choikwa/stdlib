
# tethercode compiler library - tetherasm/instructiongen.tc
# Copyright (c) 2017,
# tethercode dev team (see AUTHORS.md / https://tethercode.io/go/authors )
#
# This software is provided 'as-is', without any express or implied
# warranty. In no event will the authors be held liable for any damages
# arising from the use of this software.
#
# Permission is granted to anyone to use this software for any purpose,
# including commercial applications, and to alter it and redistribute it
# freely, subject to the following restrictions:
#
# 1. The origin of this software must not be misrepresented; you must not
#    claim that you wrote the original software. If you use this software
#    in a product, an acknowledgement in the product documentation would be
#    appreciated but is not required.
# 2. Altered source versions must be plainly marked as such, and must not be
#    misrepresented as being the original software.
# 3. This notice may not be removed or altered from any source distribution.
#
# SPDX-License-Identifier: Zlib

import encoding
import json
import url

import compiler.tetherasm.instruction

## A visitor which will generate intermediate tetherasm instructions when
## executed on a tethercode AST. Use compiler.semantics.SemanticAnalysis'
## intermediate_code() method to run this visitor if in doubt on how to
## use it.
class InstructionGenerator {
    let combined_warnings = []

    let instructions = []
    let inline_representation = {}
    let global_scope

    let highest_used_temporary = -1
    let func_nesting_depth = 0
    let class_nesting_depth = 0

    let instructions_in_funcs = {}
    let current_func = null
    let func_nesting_chain = []

    func init(global_scope,
            lowest_available_temporary=-1) {
        self.global_scope = global_scope
        self.highest_used_temporary = math.max(
            self.highest_used_temporary, lowest_available_temporary)
    }

    func gen_temp {
        self.highest_used_temporary += 1
        return self.highest_used_temporary
    }

    func visit_in(item) {
        if item.name == "named function definition" {
            let func_id = null
            if self.func_nesting_depth == 0 and
                    (not "storage" in item.extra or
                    item.extra["storage"]["type"] != "local_proc_ref"){
                func_id = "globalfunc_" + item.params[0]
            } else {
                func_id = "localfunc_" + item.extra["storage"]["id"] +
                    "_" + item.params[0]
            }
            assert(not func_id in self.instructions_in_funcs)
            self.current_func = func_id
            self.instructions_in_funcs[func_id] = []
            self.func_nesting_chain.add(func_id)
            self.func_nesting_depth += 1
        }
    }

    ## Add an instruction in the current context
    func add_inst(i) {
        if self.current_func == null {
            self.add_inst(i)
        } else {
            self.instructions_in_funcs[self.current_func].add(i)
        }
    }

    func visit_out(item) {
        assert(item != null)

        let detail = ""
        if item.name == "basic literal" and item.params[0] ==
                "identifier" {
            detail = " identifier " + item.params[1]
        }

        if item.name == "basic literal" and
                item.params[0] == "identifier" and
                "storage" in item.extra { # basic literal WITH storage
            if item.extra["storage"]["type"] == "temporary" {
                self.inline_representation[item.id] =
                    compiler.tetherasm.instruction.Instruction(
                        "t" + item.extra["storage"]["id"],
                        expr=item)
                return
            } elseif item.extra["storage"]["type"] == "global" {
                self.inline_representation[item.id] =
                    compiler.tetherasm.instruction.Instruction(
                        "g" + item.extra["storage"]["id"],
                        expr=item)
                return
            } elseif item.extra["storage"]["type"] ==
                    "external_global" {
                self.inline_representation[item.id] =
                    compiler.tetherasm.instruction.Instruction(
                        "EXTGLOBAL",
                        item.extra["storage"]["reference"]["file"],
                        item.extra["storage"]["reference"]["name"],
                        expr=item)
                return
            } elseif item.extra["storage"]["type"] ==
                    "imported module path" {
                # Nothing to do.
                return
            } elseif item.extra["storage"]["type"] ==
                    "builtin" {
                self.inline_representation[item.id] =
                    compiler.tetherasm.instruction.Instruction(
                        "BUILTIN",
                        item.params[1],
                        expr=item)
                return
            }
            throw RuntimeException(
                "unknown variable storage type encountered: " +
                item.extra["storage"]["type"])
        } elseif item.name == "basic literal" and
                item.params[0] == "identifier" {
            
        } elseif item.name == "function call" {
            let callee_id = item.params[0].id
            let callee_ref = compiler.tetherasm.instruction.Ref(
                self,
                from_dict=self.inline_representation,
                from_dict_key=callee_id)

            assert(item.params[1].params != null)

            # Collect argument expressions:
            let param_ids = []
            let keyword_args = {}
            for param in item.params[1].params {
                if param.name.endswith(
                        "keyword argument") {
                    keyword_args[param.params[0]] =
                        param.params[1].id
                } else {
                    param_ids.add(
                        param.params[0].id)
                }
            }

            # Create number of positional arguments and keyword arguments:
            let pos_arg_count_tmp = self.gen_temp()
            self.add_inst(
                compiler.tetherasm.instruction.Instruction("NEW",
                    compiler.tetherasm.instruction.Ref(self,
                        "t" + pos_arg_count_tmp),
                    "NUMLITERAL",
                    param_ids.length,
                    expr=item))
            let key_arg_count_tmp = self.gen_temp()
            self.add_inst(
                compiler.tetherasm.instruction.Instruction("NEW",
                    compiler.tetherasm.instruction.Ref(self,
                        "t" + key_arg_count_tmp),
                    "NUMLITERAL",
                    keyword_args.length,
                    expr=item))

            # Get return value slot:
            let return_value_tmp = self.gen_temp()

            # Create strings for each keyword argument:
            let kw_arg_name_tmps = {}
            for kw_arg_name in keyword_args {
                let kw_arg_name_tmp = self.gen_temp()
                kw_arg_name_tmps[kw_arg_name] = kw_arg_name_tmp
                self.add_inst(
                    compiler.tetherasm.instruction.Instruction("NEW",
                        compiler.tetherasm.instruction.Ref(self,
                            "t" + kw_arg_name_tmp),
                        "STRINGLITERAL",
                        kw_arg_name,
                        expr=item))
            }

            # Put positional arguments on top of stack:
            for p_id in param_ids {
                let pos_move_ref = compiler.tetherasm.instruction.Ref(
                    self,
                    from_dict=self.inline_representation,
                    from_dict_key=p_id)
                self.add_inst(
                    compiler.tetherasm.instruction.Instruction(
                        "COPYTOP",
                        pos_move_ref,
                        expr=item))
            }

            # Put keyword arguments on top of stack:
            for p_name in keyword_args {
                let p_id = keyword_args[p_name]
                let pos_move_ref = compiler.tetherasm.instruction.Ref(
                    self,
                    from_dict=self.inline_representation,
                    from_dict_key=p_id)
                self.add_inst(
                    compiler.tetherasm.instruction.Instruction(
                        "COPYTOP",
                        pos_move_ref,
                        expr=item))
            }

            # Create strings for each keyword argument:
            let kw_arg_tmps = {}
            for kw_arg_name in keyword_args {
                let kw_arg_name_tmp = self.gen_temp()
                kw_arg_tmps[kw_arg_name] = kw_arg_name_tmp
                self.add_inst(
                    compiler.tetherasm.instruction.Instruction("NEW",
                    compiler.tetherasm.instruction.Ref(self,
                        "t" + kw_arg_name_tmp),
                        "STRINGLITERAL",
                        kw_arg_name,
                        expr=item))
            }

            # Put positional arg count on top of stack:
            self.add_inst(
                compiler.tetherasm.instruction.Instruction("COPYTOP",
                compiler.tetherasm.instruction.Ref(self, "t" +
                    pos_arg_count_tmp),
                expr=item))

            # Put keyword arg count on top of stack:
            self.add_inst(
                compiler.tetherasm.instruction.Instruction("COPYTOP",
                    compiler.tetherasm.instruction.Ref(self, "t" +
                        key_arg_count_tmp),
                        expr=item))

            # Add call instruction:
            self.add_inst(
                compiler.tetherasm.instruction.Instruction("CALLVAR",
                    compiler.tetherasm.instruction.Ref(self,
                        "t" + return_value_tmp),
                    callee_ref,
                    expr=item))

            self.inline_representation[item.id] =
                compiler.tetherasm.instruction.Instruction("t" +
                    return_value_tmp)
        } elseif item.name == "let statement" {
            let assigned_label = item.params[0]

            if item.params.length > 2 {
                throw RuntimeException(
                    "let statements must be separated from " +
                    "assigned values in some previous stage!")
            }

            # Nothing assigned in let statement, so this is
            # only relevant for lexical scoping and not
            # for instruction generation at this point.
            return
        } elseif item.name == "basic literal" {
            let literal_tmp = self.gen_temp()
            if item.params[0] == "number" {
                self.inline_representation[item.id] =
                    compiler.tetherasm.instruction.Instruction("NEW",
                        compiler.tetherasm.instruction.Ref(self,
                            "t" + literal_tmp),
                        "NUMLITERAL",
                        encoding.parse_literal(item.params[1]),
                        location=item.start_location)
            } elseif item.params[0] == "null" {
                self.inline_representation[item.id] =
                    compiler.tetherasm.instruction.Instruction("NEW",
                        compiler.tetherasm.instruction.Ref(self,
                            "t" + literal_tmp),
                        "nullliteral",
                        location=item.start_location)
            } elseif item.params[0] == "boolean" {
                self.inline_representation[item.id] =
                    compiler.tetherasm.instruction.Instruction("NEW",
                        compiler.tetherasm.instruction.Ref(self,
                            "t" + literal_tmp),
                        "BOOLLITERAL",
                        encoding.parse_literal(item.params[1]),
                        location=item.start_location)
            } elseif item.params[0] == "string" {
                self.inline_representation[item.id] =
                    compiler.tetherasm.instruction.Instruction("NEW",
                        compiler.tetherasm.instruction.Ref(self,
                            "t" + literal_tmp),
                        "STRINGLITERAL",
                        encoding.parse_literal(item.params[1]),
                        location=item.start_location)
            } elseif item.params[0] == "bytes" {
                self.inline_representation[item.id] =
                    compiler.tetherasm.instruction.Instruction("NEW",
                        compiler.tetherasm.instruction.Ref(self,
                            "t" + literal_tmp),
                        "BYTESLITERAL",
                        encoding.parse_literal(item.params[1]),
                        location=item.start_location)
            } else {
                throw RuntimeException("unexpected literal " +
                    "type encountered: " + tostring(
                    item.params[0]))
            }
        } elseif item.name == "binary operator" {
            if item.params[0] == "." {  # member access
                assert(item.params[1] != null)
                assert(item.params[2] != null)

                # Ensure it adheres to <*>.<identifier expr>:
                if item.params[2].name != "basic literal" or
                        item.params[2].params[0] != "identifier" {
                    let what = "'" + item.params[2].name + "'"
                    if item.params[2].name == "basic literal" {
                        what = what + " (with sub type '" +
                            item.params[2].params[0] + "')"
                    }
                    return {
                        "warnings" : self.combined_warnings,
                        "errors" : [{
                            "text" : "unexpected expression of type " +
                                what + ": expected " +
                                "identifier for member access",
                            "clocation" : precconst_currpos,
                            "location" : item.start_location,
                            "label" : "semantics",
                        }],
                        "intermediate_code" : null,
                    }
                }

                # When accessing BaseType, evaluate full type name:
                if item.params[1].name == "basic literal" and
                        item.params[1].params[0] == "identifier" {
                    if "storage" in item.params[1].extra and
                            item.params[1].extra["storage"]["type"] ==
                            "builtin" and
                            item.params[1].params[1] == "BaseType" {
                        let full_type = "BaseType." + item.params[2].
                            params[1]
                        self.inline_representation[item.id] =
                            compiler.tetherasm.instruction.Instruction(
                                "BUILTIN",
                                full_type,
                                expr=item)
                        return
                    }
                }

                # Ensure we're not indexing a builtin function:
                if item.params[1].name == "basic literal" and
                        item.params[1].params[0] == "identifier" {
                    if "storage" in item.params[1].extra and
                            item.params[1].extra["storage"]["type"] ==
                            "builtin" and
                            item.params[1].params[1] != "BaseType" {
                        return {
                            "warnings" : self.combined_warnings,
                            "errors" : [{
                                "text" : "cannot access member of " +
                                    "builtin " +
                                    item.params[1].params[1],
                                "clocation" : precconst_currpos,
                                "location" : item.start_location,
                                "label" : "semantics",
                            }],
                            "intermediate_code" : null,
                        }
                    }
                }

                # For module member access, there is nothing to do here:
                if ("storage" in item.params[1].extra and
                        item.params[1].extra["storage"]["type"]
                        == "imported module path") or
                        (item.params[1].name == "binary operator" and
                        "storage" in item.params[1].params[2].extra and
                        item.params[1].params[2].extra
                        ["storage"]["type"] == "imported module path")
                        {
                    if "storage" in item.params[2].extra and
                            item.params[2].extra["storage"]["type"] ==
                            "imported module path" {
                        print("ENCOUNTERED IMPORTED MODULE PATH " +
                            "EXPR: " + tostring(item.params[2]))
                        self.inline_representation[item.id] =
                            compiler.tetherasm.instruction.Instruction(
                                "IMPORTEDEXPRPLACEHOLDER",
                                expr=item)
                        return
                    }
                    self.inline_representation[item.id] =
                        self.inline_representation[item.params[2].id]
                    return
                }

                # Insert opcode to access member:
                let repr_accessed = null
                if item.params[1].id in self.inline_representation {
                    if self.inline_representation[item.params[1].id].
                            op_code == "IMPORTEDEXPRPLACEHOLDER" {
                        # This is the final access to an imported path.
                        # The second element to the member access operator
                        # therefore must have external_global storage info:
                        let imp_path_desc = "imported module path " +
                            "<unknown>"
                        if "storage" in item.params[1].extra and
                                "full_path" in item.params[1].extra[
                                "storage"] {
                            imp_path_desc = "imported module path '" +
                                ["storage"]["full_path"] + "'"
                        } else {
                            imp_path_desc = "import module path <" +
                                "unannotated, type: " +
                                item.params[1].name + ">"
                        }

                        if item.params[1].name == "binary operator" {
                            # This is a nested module access, (a.b.).c:
                            assert(item.params[1].params[2].name ==
                                "basic literal")
                            assert(item.params[1].params[2].params[0] ==
                                "identifier")
                            imp_path_desc = "nested import module path" +
                                " ending with " +
                                item.params[1].  # CURRENT binop 1st arg
                                params[2].  # nested binop 2nd arg
                                params[1]  # basic literal 2nd arg (id)
                        }

                        assert(
                            "storage" in item.params[2].extra and
                            item.params[2].extra["storage"]["type"] ==
                            "external_global",
                            "storage of param 2 of " +
                            "binop member access '" +
                            item.params[2].params[1] +
                            "' on " + imp_path_desc +
                            " must be external_global, but this is the " +
                            "encountered .extra instead: " +
                            tostring(item.params[2].extra))
                        self.inline_representation[item.id] =
                            self.inline_representation[item.params[2].id]
                        return
                    }
                    repr_accessed = compiler.tetherasm.instruction.Ref(self,
                        self.inline_representation[item.params[1].id])
                } else {
                    let refname = item.params[1].name
                    if refname == "basic literal" {
                        refname += " " + item.params[1].params[0]
                        if item.params[1].params[0] == "identifier" {
                            refname += " " + item.params[1].params[1]
                        }
                    } elseif refname == "binary operator" {
                        refname += " " + item.params[1].params[0]
                    }
                    repr_accessed = compiler.tetherasm.instruction.
                        Instruction("UNKNOWNREF",
                        refname)
                }
                let repr_access_name = item.params[2].params[1]

                self.inline_representation[item.id] =
                    compiler.tetherasm.instruction.Instruction(
                        "BINOP_INLINE",
                        ".",
                        repr_accessed,
                        repr_access_name,
                        expr=item)
                self.add_inst(
                    self.inline_representation[item.id])
            } else {
                assert((item.params[1] != null and
                        item.params[2] != null) or item.params[0] == ":",
                    "expected binary op to have two expressions as " +
                    "children (params[1] and prams[2]) but at least " +
                    "one of those parameters is not null. " +
                    "binary op expression is: " + tostring(item))

                let operand1
                if item.params[1] != null {
                    if item.params[1].id in self.inline_representation {
                        operand1 = compiler.tetherasm.instruction.Ref(
                            self,
                            from_dict=self.inline_representation,
                            from_dict_key=item.params[1].id)
                    } else {
                        operand1 = compiler.tetherasm.instruction.
                            Instruction("UNKNOWNREF",
                            "operand1 with id " + item.params[1].id)
                    }
                } else {
                    operand1 = compiler.tetherasm.instruction.
                        Instruction("NIL")
                }
                let operand2
                if item.params[2] != null {
                    if item.params[2].id in self.inline_representation {
                        operand2 = compiler.tetherasm.instruction.Ref(
                            self,
                            from_dict=self.inline_representation,
                            from_dict_key=item.params[2].id)
                    } else {
                        operand2 = compiler.tetherasm.instruction.
                            Instruction("UNKNOWNREF",
                            "operand2 with id " + item.params[2].id)
                    }
                } else {
                    operand2 = compiler.tetherasm.instruction.
                        Instruction("NIL")
                }
                self.inline_representation[item.id] =
                    compiler.tetherasm.instruction.Instruction(
                        "BINOP_INLINE",
                        item.params[0],
                        operand1, operand2)
                self.add_inst(
                    self.inline_representation[item.id])
            }
        } elseif item.name == "assignment statement" {
            # Representation of assignment target
            let target_rep = compiler.tetherasm.instruction.Ref(
                self,
                from_dict=self.inline_representation,
                from_dict_key=item.params[0].id)

            # Representation of assigned volume
            let value_rep = compiler.tetherasm.instruction.Ref(self,
                from_dict=self.inline_representation,
                from_dict_key=item.params[1].id)

            # Generate instruction for value assignment:
            self.add_inst(
                compiler.tetherasm.instruction.Instruction("SETVALUE",
                target_rep,
                value_rep))
        } elseif item.name == "named function definition" {
            # Decrease nesting depth counter:
            self.func_nesting_depth -= 1
            assert(self.func_nesting_depth >= 0)
            let label = self.current_func

            # Set current function to the next outer one:
            self.func_nesting_chain = self.func_nesting_chain[:
                self.func_nesting_chain.length - 1]
            if self.func_nesting_chain.length > 0 {
                self.current_func = self.func_nesting_chain[
                    self.func_nesting_chain.length - 1]
            } else {
                self.current_func = null
            }

            # Emit instructions:
            self.instructions.add(
                compiler.tetherasm.instruction.Instruction(
                    "FUNCLABEL", label,
                    location=item.start_location))
            for inst in self.instructions_in_funcs[label] {
                self.instructions.add(inst)
            }
        } elseif item.name == "import statement" or
                item.name.endswith(" argument") or
                item.name.endswith(" arguments") or
                item.name == "code block" {
            # Nothing to do for these
            return
        } else {
            throw ValueException("unhandled item: " + tostring(item))
        }
    }

    func finalize_result(result) {
        assert(type(result) == BaseType.dictionary)
        result["intermediate_code"] = self.instructions
    }
}

