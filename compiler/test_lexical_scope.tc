
# tethercode - unit test
# Copyright (c) 2016-2017,
# tethercode dev team (see AUTHORS.md / https://tethercode.io/go/authors )
#
# This software is provided 'as-is', without any express or implied
# warranty. In no event will the authors be held liable for any damages
# arising from the use of this software.
#
# Permission is granted to anyone to use this software for any purpose,
# including commercial applications, and to alter it and redistribute it
# freely, subject to the following restrictions:
#
# 1. The origin of this software must not be misrepresented; you must not
#    claim that you wrote the original software. If you use this software
#    in a product, an acknowledgement in the product documentation would be
#    appreciated but is not required.
# 2. Altered source versions must be plainly marked as such, and must not be
#    misrepresented as being the original software.
# 3. This notice may not be removed or altered from any source distribution.
#
# SPDX-License-Identifier: Zlib

import io
import path

import compiler.context
import compiler.grammar.ast
import compiler.precompiler
import compiler.source
import compiler.tokenizer

func test_data_dir {
    let location = precconst_currfilepath
    return path.dirname(path.normalize(
        path.abspath(location)))
}

func _test_data(testdata, expected_result=true, debug=false) {
    let location = precconst_currfilepath
    let dir_location
    let pos = location.rfind(path.separator())
    if pos >= 0 {
        dir_location = location[:pos]
    } else {
        dir_location = ""
    }

    # Compute project root and our path seen from project root:
    let project_root_path = path.normalize(path.abspath(
        path.join(dir_location, "..")))
    let file_path = path.join(test_data_dir(),
        ".temp-test-lexical-scope-test-data.tc")
    try {
        let f = io.open(file_path, "w")
        f.write(testdata)
        f.close()

        # Create compile context:
        let context = compiler.context.create_from_location(
            project_root_path,
            limit_info=null)

        # Get unverified global scope:
        let source = compiler.source.Source(
            context, file_path,
            source_name=file_path,
            verbose=false)
        let result = source.unverified_global_scope(
            force_reparse=true, verify=true)
        if path.exists(file_path) {
            io.remove(file_path)
        }
        let storage_summary
        if result["errors"].length == 0 {
            # If no errors occured, get actual lexical scope:
            result = source.compute_lexical_scope(
                debug=debug, 
                compute_storage_summary=true)
            storage_summary = result["storage_summary"]
        }

        if expected_result {
            assert(result != null and result["errors"].length == 0,
                "test data should have been accepted, got error: " +
                tostring(result["errors"][0]))
        } else {
            assert(result != null and result["errors"].length > 0,
                "test data should NOT have been accepted, but it was!")
        }
        return storage_summary
    } catch Exception as e {
        try {
            if path.exists(file_path) {
                io.remove(file_path)
            }
        } catch Exception {
        }
        throw e
    }
    if path.exists(file_path) {
        io.remove(file_path)
    }
}

func _test_ok(t, debug=false) {
    return _test_data(t, expected_result=true, debug=debug)
}

func _test_fail(t, debug=false) {
    return _test_data(t, expected_result=false, debug=debug)
}

func test_nested_funcs_scoping {
	# Nest stuff in interesting ways:
    _test_ok('
    let y
    class abc {
        let x
        func wrap {
            func z {
                let f = x -> (y + x)
                let y = 5
                let f2 = x -> (y + x)
                f(5)
            }
            z()
        }
    }
    func main {
        let a = abc()
    }
    ')

    # Make sure direct local var shadowing is disallowed:
    _test_fail('
    let y
    class abc {
        let x
        func wrap {
            func z {
                let f = x -> (y + x)
                let f = 2
                f(5)
            }
            z()
        }
    }
    func main {
        let a = abc()
    }
    ')

    # Make sure global var shadowing is disallowed:
    _test_fail('
    let v = 1
    let v = 2
    func main {
    }
    ')

    # Make sure clashes with imported modules are disallowed:
    _test_fail('
    import math
    let math = 5
    func main {
    }')
}

func test_ok_use_self {
    _test_ok('
    class test_class {
        let a
        func init {
            self.a = 5
        }
    }
    func main {
        let v = test_class()
    }
    ')
}

## This test used to trigger a faulty shadowing error because the
## scope between the conditional blocks isn't properly cleared.
## This test makes sure it works properly.
func test_ok_if_elseif_pseudo_shadowing {
    _test_ok('
    func main {
        if true {
            let result = 0
        } elseif false {
            let result = 1
        } elseif false {
            let result = 2
        }
    }
    ')
}

## Test if deeply nested dotted member calls are handled correctly
## by lexical scope:
func test_ok_nesting_of_member_calls {
    _test_ok('
    class TestItem2 {
        func hello {
            return "hello"
        }
    }
    func _hellofunc {
        return TestItem()
    }
    class TestItem {
        func bla {
            return {
                "hello" : _hellofunc
            }
        }
    }
    func main {
        print(TestItem().bla()["hello"].hello())
    }
    ')
}

## Test if member accessing a call or constructor expression works:
func test_ok_member_access_a_call_expression {
    _test_ok('
    class TestItem {
        func bla {
            return 5
        }
    }
    func main {
        print(TestItem().bla())
    }
    ')
}



## Test that no class method definition in a class leak into
## the following class:
func test_ok_no_leaking_class_method_defs {
    _test_ok('
    class TestClass1 {
        func init(a=1, b=2) {
            print(a + b)
        }
    }
    class TestClass2 {
        func init {
        }
    }
    func main {
    }
    ')
}

## Test if storage types are correct:
func test_storage_types {
    let storage_summary = _test_ok('
    let global_var = 5
    class MyTest {
        let class_member = 3
        func test {
            let local_var = 5
            let shared_closure_var = 6
            func f {
                shared_closure_var = 5
            }
            shared_closure_var += 1
            return f
        }
    }
    ', debug=true)
    let found_global = false
    let found_member = false
    let found_local = false
    let found_closure_var = false
    for entry in storage_summary {
        if entry[0] == "global_var" {
            found_global = true
            assert(entry[1]["type"] == "global",
                "expected storage type 'global' for " +
                "variable 'global_var', got '" +
                entry[1]["type"] + "'")
        } elseif entry[0] == "class_member" {
            found_member = true
        } elseif entry[0] == "shared_closure_var" {
            found_closure_var = true
            assert(entry[1]["type"] == "closure_shared",
                "expected storage type 'closure_shared' for " +
                "variable 'shared_closure_var', got " +
                entry[1]["type"])
        } elseif entry[0] == "local_var" {
            found_local = true
            assert(entry[1]["type"] == "temporary",
                "expected storage type 'temporary' for " +
                "variable 'local_var', got " +
                entry[1]["type"])
        }
    }
    assert(found_global and not found_member and found_local,
        "expected to find 'global_var' and 'local_var' in " +
        "storage results, but not 'class_member'. instead, " +
        "got those results: " + tostring(storage_summary))
}

## Test shadowed class members don't work:
func test_fail_shadow_class_members {
    _test_fail('
    class MyTest {
        let a
        func a {
        }
    }
    func main {
    }
    ')
}

## Make sure class members can't be accessed without self:
func test_fail_invalid_class_member_reference {
    _test_fail('
    class MyTest {
        let a
        func b {
            print(a)
        }
    }
    func main {
    }
    ')
}


