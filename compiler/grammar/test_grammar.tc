
# tethercode - unit test
# Copyright (c) 2016-2017,
# tethercode dev team (see AUTHORS.md / https://tethercode.io/go/authors )
#
# This software is provided 'as-is', without any express or implied
# warranty. In no event will the authors be held liable for any damages
# arising from the use of this software.
#
# Permission is granted to anyone to use this software for any purpose,
# including commercial applications, and to alter it and redistribute it
# freely, subject to the following restrictions:
#
# 1. The origin of this software must not be misrepresented; you must not
#    claim that you wrote the original software. If you use this software
#    in a product, an acknowledgement in the product documentation would be
#    appreciated but is not required.
# 2. Altered source versions must be plainly marked as such, and must not be
#    misrepresented as being the original software.
# 3. This notice may not be removed or altered from any source distribution.
#
# SPDX-License-Identifier: Zlib

import io
import path

import compiler.context
import compiler.grammar.expression
import compiler.grammar.ast
import compiler.precompiler
import compiler.source
import compiler.tokenizer

func test_data_dir {
    let location = precconst_currfilepath
    return path.dirname(path.normalize(
        path.abspath(location)))
}

func _test(testdata, expected_success=true) {
    let location = precconst_currfilepath
    let dir_location
    let pos = location.rfind(path.separator())
    if pos >= 0 {
        dir_location = location[:pos]
    } else {
        dir_location = ""
    }

    # Compute project root and our path seen from project root:
    let project_root_path = path.normalize(path.abspath(
        path.join(dir_location, "..", "..")))
    let file_path = path.join(test_data_dir(),
        ".temp-test-grammar-test-data.tc")
    try {
        let f = io.open(file_path, "w")
        f.write(testdata)
        f.close()

        # Create compile context:
        let context = compiler.context.create_from_location(
            project_root_path,
            limit_info=null)

        # Compile source and see if it works as expected:
        let source = compiler.source.Source(
            context, file_path,
            source_name=file_path,
            verbose=false)
        result = source.unverified_global_scope(force_reparse=true)
        if path.exists(file_path) {
            io.remove(file_path)
        }
        if expected_success {
            assert(result != null and result["errors"].length == 0,
                "test data should have been accepted, got error: " +
                tostring(result["errors"][0]))
        } else {
            assert(result != null and result["errors"].length > 0,
                "test data should have been rejected, but " +
                "got no errors!")
        }
    } catch Exception as e {
        try {
            io.remove(file_path)
        } catch Exception {
        }
        throw e
    }
    if path.exists(file_path) {
        io.remove(file_path)
    }
}

func _test_ok(testdata) {
    return _test(testdata, expected_success=true)
}

func _test_fail(testdata) {
    return _test(testdata, expected_success=false)
}

func describe_item(p) {
    if p == null {
        return "<null>"
    }
    return tostring(p.name)
}

## Get AST of the given source code snippet
func _get_ast(string) {
    let tokenizer = compiler.tokenizer.Tokenizer(
        io.stream_from_string(string),
        "test_data", verbose=false)
    let tokens = tokenizer.tokens()
    assert(tokenizer.errors.length == 0)
    let result = compiler.precompiler.apply(tokens)
    assert(result.errors.length == 0,
        "expected no errors when applying precompiler, " +
        "but got those precompiler errors: " +
        tostring(result.errors))
    assert(result.tokens != null)
    tokens = result.tokens
    syntax_tree_builder = compiler.grammar.ast.TreeBuilder(
        tokens, verbose=false)
    let result = syntax_tree_builder.build()
    assert(result.errors.length == 0,
        "expected no errors when turning test string into " +
        "AST, but got those syntax parser errors: " +
        tostring(result.errors))
    return result.expression
}    

func test_chained_function_calls {
    _test_fail('
    func f(n) {
    }
    func main {
        f(1)(2)
    }')
    _test_fail('
    func f(n) {
    }
    func main {
        let a = f(1)(2)
    }')
    _test_ok('
    func f(n) {
    }
    func main {
        let a = (f(1))(2)
    }')
}

func test_tree_nested_member_access_on_import {
    # Get AST:
    let ast = _get_ast('
    import compiler.context
    func main {
        let project_base_path = ""
        let limit_info = null
        let context = compiler.context.create_from_location(
            project_base_path,
            limit_info=limit_info)
    }')
    
    # Search the "compiler" literal in the AST:
    let searcher = compiler.grammar.ast.TreeSearch(
        ast,
        limit_info=null)
    func is_startswith_expr(n) {
        return (n.name == "basic literal" and
            n.params[0] == "identifier" and
            n.params[1] == "compiler")
    }
    let items = searcher.search(is_startswith_expr)
    assert(items.length == 1,
        "test error: failed to match exactly one item with 'compiler' " +
        "as expected, instead got " + items.length + ": " +
        tostring(items))
    let compiler_item = items[0]
    
    func describe_item(p) {
        if p == null {
            return "<null>"
        }
        return tostring(p.name)
    }
    assert(compiler_item.parent != null and
        compiler_item.parent.name == "binary operator",
        "'compiler' expression expected to be child of " +
        "'binary operator' expression, but parent is '" +
        describe_item(compiler_item.parent) + "'.\n" +
        "The AST looks like this:\n" + ast.as_tree())
}

func test_tree_member_access_with_call {
    let ast = _get_ast('
    func main {
        let symbol_name = "blah"
        let e = not symbol_name.startswith("_")
    }')
    let searcher = compiler.grammar.ast.TreeSearch(ast)
    func is_startswith_expr(n) {
        return (n.name == "basic literal" and
            n.params[0] == "identifier" and
            n.params[1] == "startswith")
    }
    let items = searcher.search(is_startswith_expr)
    assert(items.length == 1,
        "test error: failed to match exactly one item with 'startswith' " +
        "as expected, instead got " + items.length + ": " +
        tostring(items))
    let startswith_item = items[0]
    assert(startswith_item.parent != null and
        startswith_item.parent.name == "binary operator",
        "'startswith' expression expected to be child of " +
        "'binary operator' expression, but parent is '" +
        describe_item(startswith_item.parent) + "'.\n" +
        "The AST looks like this:\n" + ast.as_tree())
}

func _get_expr_type_choice_for_code(code,
        parent_token_type="code block", debug=false) {
    let tokenizer = compiler.tokenizer.Tokenizer(
        io.stream_from_string(code),
        "test_data", verbose=false)
    let tokens = tokenizer.tokens()
    assert(tokenizer.errors.length == 0)
    let result = compiler.precompiler.apply(tokens)
    assert(result.errors.length == 0,
        "expected no errors when applying precompiler, " +
        "but got those precompiler errors: " +
        tostring(result.errors))
    assert(result.tokens != null)

    let code_block_parent = compiler.grammar.expression.
        Expression(parent_token_type)
    let expr_parser = compiler.grammar.expression.
        ExpressionToASTParser()

    let expr_type = expr_parser.get_matching_expr_type(result.tokens,
        code_block_parent, debug=debug)

    return expr_type
}

func test_lambda_func_inline_choice_check {
    let choice = _get_expr_type_choice_for_code(
        "let f = x -> 5")
    assert(choice != null and
        tostring(choice).lower().find("letstatement") >= 0,
        "expected let statement as grammar parsing choice, " +
        "but this was chosen: " + tostring(choice))
    choice = _get_expr_type_choice_for_code(
        "x -> 5", parent_token_type="let statement", debug=true)
    assert(choice != null and
        tostring(choice).lower().find("lambda") >= 0,
        "expected lambda function as grammar parsing choice, " +
        "but this was chosen: " + tostring(choice))
}

func test_lambda_func_in_func {
    let ast = _get_ast('
    class test_fake_obj {
        func wrap {
            let f = x -> 5
        }
    }
    func main() {
        let textwrap = test_fake_obj()
        textwrap.wrap()
    }
    ')
    let searcher = compiler.grammar.ast.TreeSearch(ast)
    func is_startswith_expr(n) {
        return (n.name == "lambda function definition")
    }
    let items = searcher.search(is_startswith_expr)
    assert(items.length == 1,
        "test error: failed to match exactly lambda function " +
        "definition " +
        "as expected, instead got " + items.length + ": " +
        tostring(items))
    let funcdef = items[0]
    assert(funcdef.parent != null and
        funcdef.parent.name == "let statement",
        "lambda function definition expected to be a child of " +
        "let statement, but parent element is " +
        describe_item(funcdef.parent) + ".\n" +
        "The AST looks like this:\n" + ast.ast_tree()) 
}

func test_ok_ifexpr_called_member_of_call_return_value {
	# Based on actual code examples:
    _test_ok('
    class test_fake_obj {
        func wrap() {
        }
    }
    func main() {
        let textwrap = test_fake_obj()
        textwrap.wrap()
    }
    ')
    _test_ok('
    func main() {
        let compile_dir
        let add_compiler_wy_packages
        if path.normalize(compiler_dir).endswith(
                path.separator_bytes() + b"wy_packages" +
                path.separator_bytes() +
                b"bin") and add_compiler_wy_packages {
            print("test")
        }
    }
    ')
}

func test_ok_call_followed_by_slicing {
    # Based on actual code examples:
    _test_ok('
    func f(v) {
    }
    func main() {
        let i
        i = f("abc")[5]
    }
    ')
} 

func test_try_finally {
    # This example used to lead to a compiler hang:
    _test_ok('
    func abc {

    }

    func func_with_finally {
        try {
            abc()
        } finally {
            abc()
        }
    }
    ')
}

## This test checks whether extending an imported class (with dots in
## the extends argument) gets parsed successfully.
func test_extend_imported_class {
    let importable_file_path = path.join(test_data_dir(),
        "my_crazy_test_module.tc")    
    try {
        let f = io.open(importable_file_path, "w")
        f.write('
        class TestClass {
        }
        ')
        f.close()
        _test_ok('
        import my_crazy_test_module

        class ExtendingClass extends my_crazy_test_module.TestClass {
        }

        func main {
            let v = ExtendingClass()
        }
        ')
    } finally {
        io.remove(importable_file_path)
    }
}

## This test checks whether complex assignments involving "self" will
## get parsed as intended. (Added because at some point they didn't)
func test_complex_assignment_with_self {
    _test_ok('
    class Test {
        let protocol
        #let abc

        func test(url_or_path) {
            if type(url_or_path) == type(self) {
                self.protocol = url_or_path.protocol
                self.abc = url_or_path.abc
            }
        }
    }

    func main {
        let a = Test()
    }
    ')
}

## Usage of async keyword:
func test_async_keyword {
    _test_ok('
    func test_func {
        print("This is a separate thread!")
    }

    func main {
        async test_func()
    }
    ')
}

## Test if deeply nested dotted member calls are handled correctly
## by grammar parser:
func test_ok_nesting_of_member_calls {
    _test_ok('
    class TestItem2 {
        func hello {
            return "hello"
        }
    }
    func _hellofunc {
        return TestItem()
    }
    class TestItem {
        func bla {
            return {
                "hello" : _hellofunc
            }
        }
    }
    func main {
        print(TestItem().bla()["hello"]().hello())
    }
    ')
}

## Test that using a positional argument after a keyword argument
## is not allowed:
func test_fail_positional_after_keyword {
    _test_fail('
    func test(a, b=5, c) {

    }
    func main {
    }
    ')
}

## Test multiarg use:
func test_ok_multiarg {
    _test_ok('
    func test(multiarg args) {

    }
    func test2(a, b) {

    }
    func main {
        test(1, 2, 3)
        test(multiarg [1, 2, 3])
        test(multiarg [1, 2])
    }
    ')
    _test_ok('
    func test(multiarg args, k=5) {

    }
    func main {
        test(1, 2, k=3)
    }
    ')
}

## Test that a positional arg can't be used after a multiarg:
func test_fail_multiarg_not_last {
    _test_fail('
    func test(multiarg args, t) {

    }
    func main {
        test(1, 2, 3)
    }
    ')
}


