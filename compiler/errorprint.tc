
# tethercode compiler library - errorprint.tc
# Copyright (c) 2017,
# tethercode dev team (see AUTHORS.md / https://tethercode.io/go/authors )
#
# This software is provided 'as-is', without any express or implied
# warranty. In no event will the authors be held liable for any damages
# arising from the use of this software.
#
# Permission is granted to anyone to use this software for any purpose,
# including commercial applications, and to alter it and redistribute it
# freely, subject to the following restrictions:
#
# 1. The origin of this software must not be misrepresented; you must not
#    claim that you wrote the original software. If you use this software
#    in a product, an acknowledgement in the product documentation would be
#    appreciated but is not required.
# 2. Altered source versions must be plainly marked as such, and must not be
#    misrepresented as being the original software.
# 3. This notice may not be removed or altered from any source distribution.
#
# SPDX-License-Identifier: Zlib

import encoding
import sys

## Print all the given warnings nicely to stdout
func print_warnings(warnings, program_name=null) {
    if program_name == null {
        program_name = path.basename(sys.argv[0])
    }
    for error in warnings {
        let label = "unlabeled"
        if "label" in error and error["label"] != null {
            label = error["label"]
        }
        let e = (program_name +
            ": warning [-W" + label + "]")
        if error["location"] {
            let source = error["location"].source
            if type(source) == BaseType.bytes {
                source = encoding.decode(source)
            }
            e += " at "
            e += source + ":"
            e += error["location"].line + ":"
            e += error["location"].column
        }
        e += ": " + error ["text"]
        print(e, stderr=true)
    }
}

## Print all the given errors to stdout and quit the program
## with exit code 1.
func terminate_with_errors(errors, debug_compiler=false,
        program_name="tethercc") {
    for error in errors {
        let label = "unlabeled"
        if "label" in error and error["label"] != null {
            label = error["label"]
        }
        let e = (program_name +
            ": error [-E" + label + "]")
        if error["location"] {
            let source = error["location"].source
            if type(source) == BaseType.bytes {
                source = encoding.decode(source)
            }
            e += " at "
            e += source + ":"
            e += error["location"].line + ":"
            e += error["location"].column
        }
        e += ": " + error ["text"]
        if debug_compiler {
            e += "\n(Internal debug info for this error: "
            if "clocation" in error {
                if not "debug" in error {
                    error["debug"] = {}
                }
                error["debug"]["clocation"] = error[
                    "clocation"]
            }
            if "debug" in error {
                e += tostring(error["debug"]) + ")"
            } else {
                e += "<no debug info present>)"
            }
        }
        print(e, stderr=true)
    }
    sys.exit(1)
}
