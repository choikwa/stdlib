
# tethercode compiler library - tccache.tc
# Copyright (c) 2017,
# tethercode dev team (see AUTHORS.md / https://tethercode.io/go/authors )
#
# This software is provided 'as-is', without any express or implied
# warranty. In no event will the authors be held liable for any damages
# arising from the use of this software.
#
# Permission is granted to anyone to use this software for any purpose,
# including commercial applications, and to alter it and redistribute it
# freely, subject to the following restrictions:
#
# 1. The origin of this software must not be misrepresented; you must not
#    claim that you wrote the original software. If you use this software
#    in a product, an acknowledgement in the product documentation would be
#    appreciated but is not required.
# 2. Altered source versions must be plainly marked as such, and must not be
#    misrepresented as being the original software.
# 3. This notice may not be removed or altered from any source distribution.
#
# SPDX-License-Identifier: Zlib

import io
import json
import path
import url

import compiler.grammar.expression
import compiler.location
import compiler.tokenizer

class TcCacheJsonTranslator {
    func extra_translate_to_json(v) {
        if type(v) == compiler.grammar.expression.
                Expression {
            return {
                "ast_type" : "Expression",
                "name" : v.name,
                "used_up_tokens" : v.used_up_tokens,
                "params" : v.params,
                "start_location" : v.start_location,
                "extra" : v.extra,
                "id" : v.id, 
            }
        } elseif type(v) == compiler.location.Location {
            let v_new = v.to_json_object()
            v_new["ast_type"] = "Location"
            return v_new
        } elseif type(v) == compiler.tokenizer.Token {
            let v_new = {
                "ast_type" : "compiler.tokenizer.Token",
                "type" : v.type,
                "contents" : v.contents,
                "location" : v.location,
                "extra_info" : v.extra_info,
            }
            return v_new
        }
        return v
    }

    func dump(v) {
        return json.dump(v,
            extra_translate_func=self.extra_translate_to_json) 
    }

    func special_parse_func(v) {
        if type(v) == BaseType.dictionary and "ast_type" in v {
            if v["ast_type"] == "Expression" {
                let v_new = compiler.grammar.
                    expression.Expression(v["name"])
                v_new.used_up_tokens = v["used_up_tokens"]
                v_new.params = v["params"]
                v_new.start_location = v["start_location"]
                v_new.extra = v["extra"]
                v_new.id = v["id"]
                if type(v_new["params"]) == BaseType.list {
                    for p in v_new["params"] {
                        if type(p) == compiler.grammar.expression.
                                Expression {
                            p.parent = v_new
                        }
                    }
                }
                return v_new
            } elseif v["ast_type"] == "Location" {
                return compiler.location.
                    create_location_from_json_object(
                    v)
            } elseif v["ast_type"] == "compiler.tokenizer.Token" {
                let v_new = compiler.tokenizer.Token(v["contents"])
                v_new.type = v["type"]
                v_new.location = v["location"]
                v_new.extra_info = v["extra_info"]
                return v_new
            }
        }
        return v
    }

    func parse(json_str) {
        return json.parse(v, translate_func=self.special_parse_func)
    }
}

class TcCacheStorage {
    let cache_per_file = {}

    func get(file_path) {
        let furl = url.Url(file_path, as_bytes=true)
        return self.cache_per_file[furl.urlstr(as_bytes=true)]
    }

    func set(file_path, value) {
        let furl = url.Url(file_path, as_bytes=true)
        self.cache_per_file[furl.urlstr(as_bytes=true)] = value
    }

    func remove(file_path) {
        let furl = url.Url(file_path, as_bytes=true)
        self.cache_per_file.remove(furl.urlstr(as_bytes=true))
    }

    func check(file_path) {
        let furl = url.Url(file_path, as_bytes=true)
        return furl.urlstr(as_bytes=true) in self.cache_per_file
    }
}

let global_cache = TcCacheStorage()

## Statistics for cache use (for debugging purposes)
class TcCacheStats {
    let ast_accesses = 0
    let wipes = 0
    let rebuilds = 0
    let saves = 0

    func tostring {
        return "TcCacheStats: AST accesses: " +
            self.ast_accesses +
            ", forced wipes: " + self.wipes +
            ", cache rebuilds: " + self.rebuilds +
            ", cache saves: " + self.saves
    }
}

## A manager for the tethercode compiler cache. Allows retrieving various
## information about .tc files on disk which will be returned from a cache
## if already available.
##
## This allows avoiding recompilation of all files every time a big project
## is compiled!
class TcCache {
    let context
    let file_url
    let cache_file_url
    let verbose

    let stats

    let cache_json_obj = null

    func init(context, file_url, verbose=false) {
        self.stats = TcCacheStats()

        self.verbose = verbose
        self.context = context
        self.file_url = url.Url(file_url, as_bytes=true)

        if self.verbose {
            print("tethercc: verbose: TcCache: initializing for " +
                self.file_url.urlstr())
        }

        let result = self.context.
            get_tccache_path_by_file_path(self.file_url)
        if result != null {
            self.cache_file_url = url.Url(result, as_bytes=true)
        }

        # Make sure we have file:// urls:
        if self.file_url.protocol != "file" {
            throw ValueException(
                "unsupported protocol: " + tostring(self.file_url.protocol))
        } elseif self.cache_file_url != null and
                self.cache_file_url.protocol != "file" {
            throw ValueException(
                "unsupported protocol: " + tostring(
                self.cache_file_url.protocol))
        }

        if self.verbose {
            if self.cache_file_url != null {
                print("tethercc: verbose: TcCache: " +
                    "computed this cache url: " +
                    self.cache_file_url.urlstr())
            } else {
                print("tethercc: verbose: TcCache: " +
                    "computed this cache url: null")
            }
        }
    }

    func update {
        if self.verbose {
            print("tethercc: verbose: TcCache: checking update for " +
                self.file_url.urlstr())
        }

        # Abort if we don't have a cache file:
        if self.cache_file_url == null {
            return
        }

        # Check if cache dir exists:
        let cache_dir = path.dirname(self.cache_file_url.resource)
        if not path.exists(cache_dir) {
            try {
                io.mkdir(cache_dir)
            } catch Exception as e {
                if self.verbose {
                    print("tethercc: verbose: TcCache: ABORTING, " +
                        "failed " +
                        "to create cache directory: " +
                        tostring(cache_dir) + " (with error " +
                        tostring(e) + ")")
                }
                return
            }
        } elseif not path.isdir(cache_dir) {
            if self.verbose {
                print("tethercc: verbose: TcCache: ABORTING, " +
                    "cache directory is not a directory: " +
                    tostring(cache_dir))
            }
            return
        }

        # Check if cache is up-to-date:
        let up_to_date_cache = true
        if not path.exists(self.cache_file_url.resource) or
                path.isdir(self.cache_file_url.resource) {
            up_to_date_cache = false
        } elseif path.mtime(self.cache_file_url.resource) <
                path.mtime(self.file_url.resource) {
            up_to_date_cache = false
        }

        let urlstr = self.cache_file_url.urlstr(as_bytes=true)

        # Wipe from memory cache if not up-to-date:
        if not up_to_date_cache {
            self.stats.wipes += 1
            if self.verbose {
                print("tethercc: verbose: TcCache: forcing " +
                    "refresh for " +
                    self.file_url.urlstr())
            }
            if global_cache.check(urlstr) {
                global_cache.remove(urlstr)
            }
            if path.exists(self.cache_file_url.resource) {
                if not path.isdir(self.cache_file_url.resource) {
                    if self.verbose {
                        print("tethercc: verbose: TcCache: wiping " +
                            "outdated disk file " +
                            self.cache_file_url.urlstr())
                    }
                    io.remove(self.cache_file_url.resource)
                }
            }
        }

        # Reload if not present in memory, but disk file there:
        if not global_cache.check(urlstr) and
                path.exists(self.cache_file_url.resource) {
            self.stats.rebuilds += 1
            let f = io.open(self.cache_file_url.resource, "r")
            try {
                translator = TcCacheJsonTranslator()
                self.cache_json_obj = translator.parse(f.read())
            } catch Exception as e {
                if self.verbose {
                    print("tethercc: verbose: TcCache: failed " +
                        "to load file " +
                        self.cache_file_url.urlstr() +
                        " due to error: " + tostring(e))
                }
            } finally {
                f.close()
            }
        }

        # Set cache value:
        if self.cache_json_obj == null {
            self.cache_json_obj = {
                "format" : 1,
                "ast" : null,
            }
        }
        global_cache.set(urlstr, self.cache_json_obj)
    }

    func retrieve_ast() {
        if self.cache_file_url == null {
            return null
        }
        self.update()
        if self.cache_json_obj != null and
                 "ast" in self.cache_json_obj {
            self.stats.ast_accesses += 1
            return self.cache_json_obj["ast"]
        }
        return null
    }

    func store_ast(ast) {
        if self.cache_file_url == null or
                self.cache_json_obj == null {
            return null
        }
        self.update()
        self.cache_json_obj["ast"] = ast

        # Set cache to memory storage and save to disk:
        let urlstr = self.cache_file_url.urlstr(as_bytes=true)
        global_cache.set(urlstr, self.cache_json_obj)
        self.save()
    }

    func save {
        if self.verbose {
            print("tethercc: verbose: TcCache: rewriting " +
                "cache: " +
                self.cache_file_url.urlstr())
        }
        self.stats.saves += 1

        # Write to file:
        let f = io.open(self.cache_file_url.resource, "w")
        try {
            translator = TcCacheJsonTranslator()
            f.write(translator.dump(self.cache_json_obj))
        } finally {
            f.close()
        }
    }
}




