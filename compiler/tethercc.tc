
# tethercode compiler library - tethercc.tc
# Copyright (c) 2016-2017,
# tethercode dev team (see AUTHORS.md / https://tethercode.io/go/authors )
#
# This software is provided 'as-is', without any express or implied
# warranty. In no event will the authors be held liable for any damages
# arising from the use of this software.
#
# Permission is granted to anyone to use this software for any purpose,
# including commercial applications, and to alter it and redistribute it
# freely, subject to the following restrictions:
#
# 1. The origin of this software must not be misrepresented; you must not
#    claim that you wrote the original software. If you use this software
#    in a product, an acknowledgement in the product documentation would be
#    appreciated but is not required.
# 2. Altered source versions must be plainly marked as such, and must not be
#    misrepresented as being the original software.
# 3. This notice may not be removed or altered from any source distribution.
#
# SPDX-License-Identifier: Zlib

import argparse
import encoding
import io
import json
import path
import platform
import program_version
import sys

import compiler.tetherasm.tetherasm
import compiler.context
import compiler.errorprint
import compiler.helpers
import compiler.precompiler
import compiler.source
import compiler.tccache
import compiler.tokenizer

## Extract all function definitions in the given tethercode file
## and return a list with all the functions' names.
## @param source_path the given tethercode source code file
## @param project_base_path base folder of the given tethercode
##                          project where tc_packages is located
##                          (for proper import processing)
## @param limit_info Resource limits to adhere to while parsing.
## @param internal_debug_compiler whether to emit detailed internal
##                                compiler info for compilation errors
func get_function_list_from_file(source_path, project_base_path,
        limit_info=null, internal_debug_compiler=false) {
    # Create compiler context and source object:
    let context = compiler.context.create_from_location(
        project_base_path,
        limit_info=limit_info)
    let source = compiler.source.Source(
        context, source_path,
        source_name=source_path,
        verbose=false)

    # Get unverified global scope (without imports) of file:
    let result = source.unverified_global_scope(
        force_reparse=false,
        verify=internal_debug_compiler)

    # Handle errors;
    if result["errors"].length > 0 {
        let e = result["errors"][0]
        let t = "error [-E"
        if "label" in e and e["label"] != null {
            t += e["label"]
        } else {
            t += "<unlabelled>"
        }
        let fname = path.basename(source_path)
        if type(fname) == BaseType.bytes {
            fname = encoding.decode(fname)
        }
        t += "] at " + fname + ":"
        if "location" in e and e["location"] != null {
            t += e["location"].line + ":" + e["location"].column + ": "
        } else {
            t += "?:?: "
        }
        if "text" in e and e["text"] != null {
            t += e["text"]
        } else {
            t += "<no error description>"
        }
        if internal_debug_compiler {
            if "debug" in e {
                t += " (debug info: " + tostring(e["debug"]) + ")"
            } else {
                t += " (debug info: none)"
            }
        }
        throw ValueException("failed to compile source " +
            "for obtaining global scope: " + t)
    }

    # Extract functions from the obtained scope of the file:
    let funcs = []
    for symbol_name in result["global_scope"]["symbols"] {
        let symbol = result["global_scope"]["symbols"][symbol_name]
        if symbol["type"] == "named function definition" and
                not symbol_name.startswith("_") {
            funcs.add(symbol_name)
        }
    }
    return funcs
}

## Output detailed version info for the compiler to stdout and
## quit the program with exit code 0.
func print_version {
    print("tethercode standalone compiler")
    print("Copyright (C) Jonas Thiem et al.")
    print("  Program version:      " + program_version.PROGRAM_VERSION)
    print("  Detected platform:    " + tostring(platform.system()))
    print("  Runtime information:  " + tostring(platform.runtime()))
    sys.exit(0)
}

## This is the main entrypoint of the tethercc executable.
func main {
    let argparser = argparse.ArgumentParser(
        description="tethercode standalone compiler",
        program_name="tethercc")
    argparser.add_argument("file", help="The code file to be compiled")
    argparser.add_argument("--version",
        help="Print program version info and exit",
        action=print_version,
        aliases=["-V"])
    argparser.add_argument("--internal-debug-compiler",
        help="Print internal debug info used by the compiler developers " +
            "for debugging the compiler itself", dest="debug_compiler")
    argparser.add_argument("--intermediate-result",
        nargs=1,
        help="Don't produce a final binary, but stop with the specified " +
            "intermediate stage output. Possible stages to be specified " +
            "are: \"tokenizer\" (produces .tok.txt file), " +
            "\"precompiler\" (produces .postprec.tc file), " +
            "\"ast\" (produces .ast.json file), " +
            "\"tetherasm\" (produces .tetherasm file) and " +
            "\"c\" (produces a .c file)",
        dest="intermediate_result")
    argparser.add_argument("--output",
        nargs=1,
        help="Output file. Defaults to '<basename>', or respectively " +
            "'<basename>.tok.txt', '<basename>.postprec.tc', " +
            "'<basename>.ast.json', " +
            "'<basename>.tetherasm', " +
            ".. depending on the stage specified with --intermediate-result",
        dest="output")
    argparser.add_argument("-v",
        help="Give verbose output about the compilation process",
        aliases=["--verbose"], dest="verbose")
    argparser.add_argument("--verbose-tokenization",
        help="Give very verbose output including detailed " +
            "tokenization of all files (warning: can be VERY long)",
        dest="verbose_tokenization")
    let args = argparser.parse_args()

    # Reject invalid intermediate results possibly specified by user:
    if args["intermediate_result"] != null {
        if not args["intermediate_result"] in {<
                "tokenizer", "intermediate",  "precompiler",
                "ast", "tetherasm", "c"
                >} {
            print(path.basename(sys.argv[0]) +
                ": error: intermediate stage not known: " +
                args["intermediate_result"], stderr=true)
            sys.exit(1)
        }
    }

    # Check if input file actually exists:
    if not path.exists(args["file"]) {
        print(path.basename(sys.argv[0]) +
            ": error: file not found: " + args["file"],
            stderr=true)
        sys.exit(1)
    } elseif path.isdir(args["file"]) {
        print(path.basename(sys.argv[0]) +
            ": error: is a directory, not a file: " + args["file"],
            stderr=true)
        sys.exit(1)
    }

    # Open up file:
    if args["verbose"] {
        print("tethercc: info: verbose: opening file for " +
            "compilation: " + args["file"])
    }
    let file = io.open(args["file"], "r")
    if file == null {
        print(path.basename(sys.argv[0]) +
            ": error: failed to open file: " + args["file"],
            stderr=true)
        sys.exit(1)
    }

    # If target is tokenization, just generate it and stop:
    if args["intermediate_result"] == "tokenizer" {
        let f = io.open(args["file"], "r")
        let tokenizer = compiler.tokenizer.Tokenizer(
            f, args["file"],
            verbose=args["verbose_tokenization"])
        let tokens = tokenizer.tokens()
        if tokenizer.warnings.length > 0 {
            compiler.errorprint.print_warnings(tokenizer.warnings)
        }
        if tokenizer.errors.length > 0 {
            compiler.errorprint.terminate_with_errors(
                tokenizer.errors,
                debug_compiler=args["debug_compiler"])
        }
        
        let output_path = compiler.helpers.fix_output_path(
            args["file"], args["output"], output_extension=".tok.txt")
        try {
            f = io.open(output_path, "w")
        } catch Exception {
            print(path.basename(sys.argv[0]) +
                ": error: failed to open output path " +
                "for writing: " + tostring(output_path),
                stderr=true)
            sys.exit(1)
        }
        for token in tokens {
            f.write("token[t=" + token.type + ",l=" +
                token.location.line + ",c=" +
                token.location.column + "]: " + token.contents +
                platform.linebreak())
        }
        f.close()
        print(path.basename(sys.argv[0]) +
            ": info: tokenization written to: " + encoding.decode(
            output_path))
        sys.exit(0)
    }

    # If target is precompilation, just generate it and stop:
    if args["intermediate_result"] == "precompiler" {
        let f = io.open(args["file"], "r")

        # Get tokenization first:
        let tokenizer = compiler.tokenizer.Tokenizer(
            null, f, args["file"],
            verbose=args["verbose_tokenization"])
        let tokens = tokenizer.tokens()
        if tokenizer.warnings.length > 0 {
            compiler.errorprint.print_warnings(tokenizer.warnings)
        }
        if tokenizer.errors.length > 0 {
            compiler.errorprint.terminate_with_errors(
                tokenizer.errors,
                debug_compiler=args["debug_compiler"])
        }

        # Apply precompiler:
        let result = compiler.precompiler.apply(tokens)
        if result.warnings.length > 0 {
            compiler.errorprint.print_warnings(result.warnings)
        }
        if result.errors.length > 0 {
            return {
                "errors" : result.errors,
                "warnings" : result.warnings,
            }
        }

        # Write output file:
        let output_path = compiler.helpers.fix_output_path(
            args["file"], args["output"], output_extension=".postprec.tc")
        try {
            f = io.open(output_path, "w")
        } catch Exception {
            print(path.basename(sys.argv[0]) +
                ": error: failed to open output path " +
                "for writing: " + tostring(output_path),
                stderr=true)
            sys.exit(1)
        }
        f.write(compiler.tokenizer.print_code(
            tokens, pretty=true))
        f.close()
        print(path.basename(sys.argv[0]) +
            ": info: precompiler result written to: " +
            encoding.decode(
            output_path))
        sys.exit(0)
    }

    # If target is AST, just generate it and stop:
    if args["intermediate_result"] == "ast" {
        # Compile syntax tree:
        let context = compiler.context.create_from_location(
            path.dirname(args["file"]))
        let source = compiler.source.Source(
            context, file,
            source_name=args["file"],
            verbose=args["verbose"],
            verbose_tokenization=args["verbose_tokenization"])
        let result = source.build_local_ast(force_reparse=true)
        if result["warnings"].length > 0 {
            compiler.errorprint.print_warnings(result["warnings"])
        }
        if result["errors"].length > 0 {
            compiler.errorprint.terminate_with_errors(
                result["errors"],
                debug_compiler=args["debug_compiler"])
        }

        # Write output file:
        let output_path = compiler.helpers.fix_output_path(
            args["file"], args["output"], output_extension=".ast.json")
        let translator = compiler.tccache.TcCacheJsonTranslator()
        let f
        try {
            f = io.open(output_path, "w")
        } catch Exception {
            print(path.basename(sys.argv[0]) +
                ": error: failed to open output path " +
                "for writing: " + tostring(output_path),
                stderr=true)
            sys.exit(1)
        }
        f.write(json.prettify(translator.dump(
            source.abstract_syntax_tree)) + "\n")
        f.close()
        print(path.basename(sys.argv[0]) +
            ": info: serialized AST written written to: " +
            encoding.decode(
            output_path))
        sys.exit(0)
    }

    # If target is tetherasm code, just generate it and stop:
    if args["intermediate_result"] == "tetherasm" {
        # Compile intermediate code:
        let context = compiler.context.create_from_location(
            path.dirname(args["file"]))
        let source = compiler.source.Source(
            context, file,
            source_name=args["file"],
            verbose=args["verbose"],
            verbose_tokenization=args["verbose_tokenization"])
        let result = source.intermediate_code()
        if result["warnings"].length > 0 {
            compiler.errorprint.print_warnings(result["warnings"])
        }
        if result["errors"].length > 0 {
            compiler.errorprint.terminate_with_errors(
                result["errors"],
                debug_compiler=args["debug_compiler"])
        }

        # Write output file:
        let output_path = compiler.helpers.fix_output_path(
            args["file"], args["output"], output_extension=".tetherasm")
        let f
        try {
            f = io.open(output_path, "w")
        } catch Exception {
            print(path.basename(sys.argv[0]) +
                ": error: failed to open output path " +
                "for writing: " + tostring(output_path),
                stderr=true)
            sys.exit(1)
        }

        # Write code nicely formatted to file:
        let nice_code = compiler.tetherasm.tetherasm.pretty_print_code(
            result["intermediate_code"])
        f.write(platform.linebreak() + nice_code.trim() + platform.linebreak())
        if platform.linebreak() == "\n" {
            # Add POSIX line break at end of file
            f.write("\n")
        }
        f.close()

        # Output user info about the completed process and quit
        print(path.basename(sys.argv[0]) +
            ": info: intermediate tetherasm written to: " + encoding.decode(
            output_path))
        sys.exit(0)
    }

    # Compile file to tetherasm:
    let context = compiler.context.create_from_location(
        path.dirname(args["file"]))
    let source = compiler.source.Source(
        context, file,
        source_name=args["file"],
        verbose=args["verbose"],
        verbose_tokenization=args["verbose_tokenization"])
    let result = source.intermediate_code()
    if result["warnings"].length > 0 {
        compiler.errorprint.print_warnings(result["warnings"])
    }
    if result["errors"].length > 0 {
        compiler.errorprint.terminate_with_errors(
            result["errors"],
            debug_compiler=args["debug_compiler"])
    }

    # If target is C code, just generate it and stop:
    if args["intermediate_result"] == "c" {
        # Translate to C code:
        let result = compiler.tetherasm.tetherasm.tetherasm_to_c(
            result["intermediate_code"],
            source_file_name=args["file"],
            verbose=args["verbose"],
            program_name="tethercc")
        if result["errors"].length > 0 {
            compiler.errorprint.terminate_with_errors(
                result["errors"], program_name="tethercc")
        }
        if result["warnings"].length > 0 {
            compiler.errorprint.print_warnings(
                result["warnings"], program_name="tethercc")
        }
        
        # Write output file:
        let output_path = compiler.helpers.fix_output_path(
            args["file"], args["output"], output_extension=".c")
        let f
        try {
            f = io.open(output_path, "w")
        } catch Exception {
            print(path.basename(sys.argv[0]) +
                ": error: failed to open output path " +
                "for writing: " + tostring(output_path),
                stderr=true)
            sys.exit(1)
        }

        # Output user info about the completed process and quit
        print(path.basename(sys.argv[0]) +
            ": info: intermediate C written to: " + encoding.decode(
            output_path))
        sys.exit(0)
    }

    # Generate final executable with the help of a C compiler:
    let output_bin_ext = ""
    if platform.system() == "Windows" {
        output_bin_ext = ".exe"
    }
    let output_path = compiler.helpers.fix_output_path(
        args["file"],
        args["output"],
        output_extension=output_bin_ext)
    compiler.tetherasm.tetherasm.tetherasm_to_c_executable(
        result["intermediate_code"],
        output_path,
        source_file_name=args["file"],
        verbose=args["verbose"],
        debug=args["debug_compiler"])

    sys.exit(0)
}

