# Authors of the tethercode language and accompanying tools:

All authors and contributors, also referred to as
"tethercode dev team", are listed here in alphabetic order:

- Axel Lehmann (2017)
- Jonas Thiem (2016-2017)

